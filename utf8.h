
/* UTF-8 Structure var a = first byte
 a >= 240 is 4 Byte
 a >= 224 is 3 Byte
 a >= 192 is 2 Byte
 else 1 Byte */

#include <stddef.h>
#include <stdint.h>

typedef int32_t u8char_t;
typedef struct u8str_t {
	size_t length;
    u8char_t *data;
} u8str_t;

size_t u8char_byteset(u8char_t ch);

u8str_t* u8str_new(const char *str);
void u8str_free(u8str_t** obj);
size_t u8str_length(u8str_t* obj);

unsigned char* u8str_to_string(u8str_t* obj);
