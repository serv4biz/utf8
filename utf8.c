#include "utf8.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>

size_t u8char_byteset(u8char_t ch) {
	unsigned char* point;
	point = (unsigned char*)&ch;
	if (*point >= 240) {
		return 4;
	} else if (*point >= 224) {
		return 3;
	} else if (*point >= 192) {
		return 2;
	}
	return 1;
}

u8str_t* u8str_new(const char *str) {
	unsigned char *pstr = (unsigned char*) str;
	if (pstr == NULL) {
		pstr = (unsigned char*) &"";
	}

	unsigned int size = strlen((const char*) pstr);
	u8char_t buffers[size];

	size_t length = 0;
	size_t i = 0;
	size_t count = 0;
	size_t countByte = 0;
	unsigned char bytes[sizeof(u8char_t)];
	for (i = 0; i < size; i++) {
		if (pstr[i] >= 240 && countByte <= 0) {
			countByte = 4;
		} else if (pstr[i] >= 224 && countByte <= 0) {
			countByte = 3;
		} else if (pstr[i] >= 192 && countByte <= 0) {
			countByte = 2;
		} else if (countByte <= 0) {
			countByte = 1;
		}

		bytes[count] = pstr[i];
		count++;

		countByte--;
		if (countByte <= 0) {
			u8char_t ch = '\0';
			memcpy(&ch, &bytes, sizeof(u8char_t));
			memset(&bytes, '\0', sizeof(u8char_t));

			buffers[length] = ch;
			count = 0;
			length++;
		}
	}

	u8str_t *obj = malloc(sizeof(u8str_t));
	obj->length = length;
	obj->data = malloc(sizeof(u8char_t) * length);
	for (i = 0; i < length; i++) {
		obj->data[i] = buffers[i];
	}
	return obj;
}

void u8str_free(u8str_t **obj) {
	(*obj)->length = 0;
	free((*obj)->data);
	free((*obj));
	(*obj) = NULL;
}

size_t u8str_length(u8str_t *obj) {
	return obj->length;
}

unsigned char* u8str_to_string(u8str_t *obj) {
	unsigned char buffers[obj->length * 4];
	size_t index = 0;

	size_t i = 0;
	for (i = 0; i < obj->length; i++) {
		unsigned char bytes[sizeof(u8char_t)];
		memcpy(&bytes, &obj->data[i], sizeof(u8char_t));

		if (bytes[0] >= 240) {
			int c = 0;
			for (c = 0; c < 4; c++) {
				buffers[index] = bytes[c];
				index++;
			}
		} else if (bytes[0] >= 224) {
			int c = 0;
			for (c = 0; c < 3; c++) {
				buffers[index] = bytes[c];
				index++;
			}
		} else if (bytes[0] >= 192) {
			int c = 0;
			for (c = 0; c < 2; c++) {
				buffers[index] = bytes[c];
				index++;
			}
		} else {
			int c = 0;
			for (c = 0; c < 1; c++) {
				buffers[index] = bytes[c];
				index++;
			}
		}
	}

	size_t size = (sizeof(unsigned char) * index) + 1;
	unsigned char* pstr = malloc(size);
	for(i=0;i<size;i++) {
		pstr[i] = buffers[i];
	}
	pstr[size-1] = '\0';
	return pstr;
}
